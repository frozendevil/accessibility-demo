//
//  ViewController.m
//  Accessibility
//
//  Created by Izzy Fraimow on 9/23/14.
//  Copyright (c) 2014 anathemacalculus. All rights reserved.
//

#import "HamburgerCollectionViewController.h"

#import "HamburgerCollectionViewCell.h"
#import "LoadingCollectionViewCell.h"
#import "HamburgerInfo.h"
#import "AboutViewController.h"
#import "HamburgerBuilderViewController.h"

static NSString * const HamburgerCellReuseIdentifier = @"HamburgerCell";
static NSString * const LoadingCellReuseIdentifier = @"LoadingCell";

static NSString * const HamburgerBuilderSegueIdentifier = @"BuildHamburger";


@interface HamburgerCollectionViewController () <UIScrollViewAccessibilityDelegate, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) NSMutableArray *hamburgers;

@property (atomic, assign) BOOL loading;

@property (nonatomic, strong) HamburgerCollectionViewCell *prototypeCell;

@end

@implementation HamburgerCollectionViewController

- (id)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	if(!self) { return nil; }
	
	_prototypeCell = [[HamburgerCollectionViewCell alloc] initWithCoder:aDecoder];
	
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	
	self.hamburgers = [NSMutableArray array];
	[self addRandomHamburgers];
	
	UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)self.collectionViewLayout;
	flowLayout.estimatedItemSize = CGSizeMake(150, 200);
	
	self.navigationItem.leftBarButtonItem.accessibilityLabel = NSLocalizedString(@"Application Info Button", nil);
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	[self.collectionView reloadData];
}

- (void)addRandomHamburgers {
	for(int i = 0; i < 20; i++) {
		[self.hamburgers addObject:[HamburgerInfo hamburgerWithRandomValues]];
	}
}

- (void)loadNextPage {
	if(self.loading) {
		return;
	}
	
	//Fake like we're making a network request
	self.loading = YES;
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	double delay = 3;
	dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_SEC);
	dispatch_after(delayTime, dispatch_get_main_queue(), ^(void){
		
		[self addRandomHamburgers];
		
		[self.collectionView reloadData];
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		self.loading = NO;
	});
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	if([segue.identifier isEqualToString:HamburgerBuilderSegueIdentifier]) {
		NSIndexPath *index = [self.collectionView indexPathForCell:sender];
		HamburgerInfo *info = self.hamburgers[index.item];
		HamburgerBuilderViewController *destination = segue.destinationViewController;
		destination.hamburgerInfo = info;
	}
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
	return self.hamburgers.count/* + 1*/;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
	UICollectionViewCell *cell;
	if(indexPath.item < self.hamburgers.count) {
		cell = [collectionView dequeueReusableCellWithReuseIdentifier:HamburgerCellReuseIdentifier forIndexPath:indexPath];
		HamburgerCollectionViewCell *hamburgerCell = (HamburgerCollectionViewCell *)cell;
		
		HamburgerInfo *info = self.hamburgers[indexPath.item];
		
		hamburgerCell.thumbnailView.numberOfPatties = info.numberOfPatties;
		hamburgerCell.thumbnailView.hasLettuce = info.hasLettuce;
		hamburgerCell.thumbnailView.hasCheese = info.hasCheese;
		
		NSInteger burgerNumber = indexPath.item + 1;
		NSString *burgerLabelText = [NSString stringWithFormat:NSLocalizedString(@"Burger #%zd", nil), burgerNumber];
		hamburgerCell.titleLabel.text = burgerLabelText;
	} else {
		cell = [collectionView dequeueReusableCellWithReuseIdentifier:LoadingCellReuseIdentifier forIndexPath:indexPath];
		LoadingCollectionViewCell *loadingCell = (LoadingCollectionViewCell *)cell;
		[loadingCell.activityIndicator startAnimating];
		
		[self loadNextPage];
	}
	
	return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
	if(![cell.reuseIdentifier isEqual:LoadingCellReuseIdentifier]) {
		return;
	}
	
	LoadingCollectionViewCell *loadingCell = (LoadingCollectionViewCell *)cell;
	[loadingCell.activityIndicator stopAnimating];
}

#pragma mark - UIScrollViewAccessibilityDelegate

- (NSString *)accessibilityScrollStatusForScrollView:(UIScrollView *)scrollView {
	return @"hello";
}

@end
