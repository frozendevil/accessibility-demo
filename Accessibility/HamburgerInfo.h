//
//  BurgerInfo.h
//  Accessibility
//
//  Created by Izzy Fraimow on 10/8/14.
//  Copyright (c) 2014 anathemacalculus. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HamburgerInfo : NSObject

@property (nonatomic, assign) IBInspectable BOOL hasCheese;
@property (nonatomic, assign) IBInspectable BOOL hasLettuce;
@property (nonatomic, assign) IBInspectable NSUInteger numberOfPatties;

+ (instancetype)hamburgerWithRandomValues;

@end
