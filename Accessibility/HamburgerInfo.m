//
//  BurgerInfo.m
//  Accessibility
//
//  Created by Izzy Fraimow on 10/8/14.
//  Copyright (c) 2014 anathemacalculus. All rights reserved.
//

#import "HamburgerInfo.h"

@implementation HamburgerInfo

+ (instancetype)hamburgerWithRandomValues {
	HamburgerInfo *info = [[self alloc] init];
	
	info.numberOfPatties = arc4random_uniform(8) + 1;
	info.hasCheese = arc4random_uniform(2) % 2 == 0;
	info.hasLettuce = arc4random_uniform(2) % 2 == 0;
	
	return info;
}

@end
