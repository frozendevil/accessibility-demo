//
//  AboutAppPresentationController.h
//  Accessibility
//
//  Created by Izzy Fraimow on 10/9/14.
//  Copyright (c) 2014 anathemacalculus. All rights reserved.
//

@import UIKit;

@interface AboutAppPresentationController : UIPresentationController

@end
