//
//  AppTheme.h
//  Accessibility
//
//  Created by Izzy Fraimow on 10/8/14.
//  Copyright (c) 2014 anathemacalculus. All rights reserved.
//

@import UIKit;

@interface AppTheme : NSObject

+ (instancetype)sharedTheme;

+ (void)animateStandard:(dispatch_block_t)animation completion:(void (^)(BOOL finished))completion;
+ (void)animatePresentation:(dispatch_block_t)animation completion:(void (^)(BOOL finished))completion;

@end
