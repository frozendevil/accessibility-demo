//
//  HamburgerBuilderView.m
//  Accessibility
//
//  Created by Izzy Fraimow on 10/9/14.
//  Copyright (c) 2014 anathemacalculus. All rights reserved.
//

#import "HamburgerBuilderView.h"

@implementation HamburgerBuilderView

- (void)commonInit {
	self.translatesAutoresizingMaskIntoConstraints = NO;
	self.isAccessibilityElement = YES;
	
	self.hasCheese = NO;
	self.hasLettuce = NO;
	self.numberOfPatties = 1;
	
#if !TARGET_INTERFACE_BUILDER
	NSBundle *bundle = [NSBundle mainBundle];
#else
	NSBundle *bundle = [NSBundle bundleForClass:[self class]];
#endif
	
	self.bunTopImage = [UIImage imageNamed:@"bun-top-big" inBundle:bundle compatibleWithTraitCollection:self.traitCollection];
	self.bunBottomImage = [UIImage imageNamed:@"bun-bottom-big" inBundle:bundle compatibleWithTraitCollection:self.traitCollection];
	self.pattyImage = [UIImage imageNamed:@"patty-big" inBundle:bundle compatibleWithTraitCollection:self.traitCollection];
	self.cheeseImage = [UIImage imageNamed:@"cheese-big" inBundle:bundle compatibleWithTraitCollection:self.traitCollection];
	self.lettuceImage = [UIImage imageNamed:@"lettuce-big" inBundle:bundle compatibleWithTraitCollection:self.traitCollection];
}


- (NSString *)accessibilityLabel {
	if(!self.hasLettuce && !self.hasCheese) {
		return [NSString stringWithFormat:NSLocalizedString(@"%@ patties, no cheese, no lettuce in current burger", nil), @(self.numberOfPatties)];
	} else if(self.hasLettuce && !self.hasCheese) {
		return [NSString stringWithFormat:NSLocalizedString(@"%@ patties, lettuce, and no cheese in current burger", nil), @(self.numberOfPatties)];
	} else if(!self.hasLettuce && self.hasCheese) {
		return [NSString stringWithFormat:NSLocalizedString(@"%@ patties, cheese, no lettuce in current burger", nil), @(self.numberOfPatties)];
	} else {
		return [NSString stringWithFormat:NSLocalizedString(@"%@ patties, cheese, and lettuce in current burger", nil), @(self.numberOfPatties)];
	}
}

@end
