//
//  HamburgerBuilderViewController.h
//  Accessibility
//
//  Created by Izzy Fraimow on 10/9/14.
//  Copyright (c) 2014 anathemacalculus. All rights reserved.
//

@import UIKit;

#import "HamburgerBuilderView.h"
#import "HamburgerInfo.h"

@interface HamburgerBuilderViewController : UIViewController

@property (nonatomic, weak) IBOutlet HamburgerBuilderView *builderView;

@property (nonatomic, strong) HamburgerInfo *hamburgerInfo;

@end
