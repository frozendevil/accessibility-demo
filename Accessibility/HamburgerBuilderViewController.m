//
//  HamburgerBuilderViewController.m
//  Accessibility
//
//  Created by Izzy Fraimow on 10/9/14.
//  Copyright (c) 2014 anathemacalculus. All rights reserved.
//

#import "HamburgerBuilderViewController.h"

@interface HamburgerBuilderViewController ()

@end

@implementation HamburgerBuilderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
	
	UISwipeGestureRecognizer *recognizer;
	recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeUp:)];
	[recognizer setDirection:(UISwipeGestureRecognizerDirectionUp)];
	[self.builderView addGestureRecognizer:recognizer];
	
	recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeDown:)];
	[recognizer setDirection:(UISwipeGestureRecognizerDirectionDown)];
	[self.builderView addGestureRecognizer:recognizer];
	
	UIAccessibilityCustomAction *addAction = [[UIAccessibilityCustomAction alloc] initWithName:NSLocalizedString(@"Add Patty", nil) target:self selector:@selector(swipeUp:)];
	UIAccessibilityCustomAction *removeAction = [[UIAccessibilityCustomAction alloc] initWithName:NSLocalizedString(@"Remove Patty", nil) target:self selector:@selector(swipeDown:)];
	
	self.builderView.accessibilityCustomActions = @[addAction, removeAction];
	
	[self updateBuilderView];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	self.navigationItem.title = NSLocalizedString(@"Edit your burger", nil);
}

- (BOOL)swipeUp:(id)sender {
	if(self.builderView.numberOfPatties < 8) {
		self.builderView.numberOfPatties++;
		self.hamburgerInfo.numberOfPatties = self.builderView.numberOfPatties;
	}

	UIAccessibilityPostNotification(UIAccessibilityLayoutChangedNotification, nil);
	
	return YES;
}

- (BOOL)swipeDown:(id)sender {
	if(self.builderView.numberOfPatties > 0) {
		self.builderView.numberOfPatties--;
		self.hamburgerInfo.numberOfPatties = self.builderView.numberOfPatties;
	}
	
	UIAccessibilityPostNotification(UIAccessibilityLayoutChangedNotification, nil);
	
	return YES;
}

- (void)setHamburgerInfo:(HamburgerInfo *)hamburgerInfo {
	if(_hamburgerInfo == hamburgerInfo) {
		return;
	}
	
	_hamburgerInfo = hamburgerInfo;
	
	[self updateBuilderView];
}

- (void)updateBuilderView {
	self.builderView.hasCheese = self.hamburgerInfo.hasCheese;
	self.builderView.hasLettuce = self.hamburgerInfo.hasLettuce;
	self.builderView.numberOfPatties = self.hamburgerInfo.numberOfPatties;
}

@end
