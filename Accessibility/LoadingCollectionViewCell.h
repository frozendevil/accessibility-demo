//
//  LoadingCollectionViewCell.h
//  Accessibility
//
//  Created by Izzy Fraimow on 10/8/14.
//  Copyright (c) 2014 anathemacalculus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
