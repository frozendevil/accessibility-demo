//
//  AboutAppPresentationController.m
//  Accessibility
//
//  Created by Izzy Fraimow on 10/9/14.
//  Copyright (c) 2014 anathemacalculus. All rights reserved.
//

#import "AboutAppPresentationController.h"

@import UIKit;

@interface AboutAppPresentationController ()

@property (nonatomic, strong) UIVisualEffectView *backgroundView;

@end

@implementation AboutAppPresentationController

- (instancetype)initWithPresentedViewController:(UIViewController *)presentedViewController presentingViewController:(UIViewController *)presentingViewController {
	self = [super initWithPresentedViewController:presentedViewController presentingViewController:presentingViewController];
	if(!self) { return nil; }
	
	UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
	_backgroundView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
	
	return self;
}

- (void)presentationTransitionWillBegin {
	self.backgroundView.frame = self.containerView.bounds;
	[self.containerView addSubview:self.backgroundView];
}

- (void)presentationTransitionDidEnd:(BOOL)completed {
	if(!completed) {
		[self.backgroundView removeFromSuperview];
	}
}

- (void)dismissalTransitionWillBegin {
	id<UIViewControllerTransitionCoordinator> coordinator = self.presentingViewController.transitionCoordinator;
	[coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
		self.backgroundView.alpha = 0;
	} completion:nil];
	
}

- (void)dismissalTransitionDidEnd:(BOOL)completed {
	if(!completed) {
		return;
	}
	
	[self.backgroundView removeFromSuperview];
}

- (CGRect)frameOfPresentedViewInContainerView {
	CGRect newFrame = self.containerView.bounds;
	newFrame = CGRectInset(newFrame, 20, 20);
	return newFrame;
}

@end
