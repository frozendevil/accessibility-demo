//
//  AppDelegate.h
//  Accessibility
//
//  Created by Izzy Fraimow on 9/23/14.
//  Copyright (c) 2014 anathemacalculus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

