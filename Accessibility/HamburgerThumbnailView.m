//
//  HamburgerThumbnailView.m
//  Accessibility
//
//  Created by Izzy Fraimow on 10/7/14.
//  Copyright (c) 2014 anathemacalculus. All rights reserved.
//

/// This class is mostly sloppy drawing code, nothing important here

#import "HamburgerThumbnailView.h"

@interface HamburgerThumbnailView ()

@end

@implementation HamburgerThumbnailView

- (instancetype)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	if(!self) { return nil; }
	
	[self commonInit];
	
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	if(!self) { return nil; }
	
	[self commonInit];
	
	return self;
}

- (void)commonInit {
	self.translatesAutoresizingMaskIntoConstraints = NO;
	
	_hasCheese = NO;
	_hasLettuce = NO;
	_numberOfPatties = 1;
	
#if !TARGET_INTERFACE_BUILDER
	NSBundle *bundle = [NSBundle mainBundle];
#else
	NSBundle *bundle = [NSBundle bundleForClass:[self class]];
#endif
	
	_bunTopImage = [UIImage imageNamed:@"bun-top" inBundle:bundle compatibleWithTraitCollection:self.traitCollection];
	_bunBottomImage = [UIImage imageNamed:@"bun-bottom" inBundle:bundle compatibleWithTraitCollection:self.traitCollection];
	_pattyImage = [UIImage imageNamed:@"patty" inBundle:bundle compatibleWithTraitCollection:self.traitCollection];
	_cheeseImage = [UIImage imageNamed:@"cheese" inBundle:bundle compatibleWithTraitCollection:self.traitCollection];
	_lettuceImage = [UIImage imageNamed:@"lettuce" inBundle:bundle compatibleWithTraitCollection:self.traitCollection];
}

- (void)setHasCheese:(BOOL)hasCheese {
	if(_hasCheese == hasCheese) {
		return;
	}
	
	_hasCheese = hasCheese;
	
	[self invalidateIntrinsicContentSize];
	[self setNeedsDisplay];
}

- (void)setHasLettuce:(BOOL)hasLettuce {
	if(_hasLettuce == hasLettuce) {
		return;
	}
	
	_hasLettuce = hasLettuce;
	
	[self invalidateIntrinsicContentSize];
	[self setNeedsDisplay];
}

- (void)setNumberOfPatties:(NSUInteger)numberOfPatties {
	if(_numberOfPatties == numberOfPatties) {
		return;
	}
	
	_numberOfPatties = numberOfPatties;
	
	[self invalidateIntrinsicContentSize];
	[self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
	CGRect drawingRect = self.bounds;
	CGRect slice;
	
	CGRectDivide(drawingRect, &slice, &drawingRect, self.bunTopImage.size.height, CGRectMinYEdge);
	[self.bunTopImage drawInRect:slice];
	
	if(self.hasCheese) {
		CGRectDivide(drawingRect, &slice, &drawingRect, self.cheeseImage.size.height, CGRectMinYEdge);
		[self.cheeseImage drawInRect:slice];
	}
	
	for(int i = 0; i < self.numberOfPatties; i++) {
		CGRectDivide(drawingRect, &slice, &drawingRect, self.pattyImage.size.height, CGRectMinYEdge);
		[self.pattyImage drawInRect:slice];
	}
	
	if(self.hasLettuce) {
		CGRectDivide(drawingRect, &slice, &drawingRect, self.lettuceImage.size.height, CGRectMinYEdge);
		[self.lettuceImage drawInRect:slice];
	}
	
	CGRectDivide(drawingRect, &slice, &drawingRect, self.bunBottomImage.size.height, CGRectMinYEdge);
	[self.bunBottomImage drawInRect:slice];
}

- (CGSize)intrinsicContentSize {
	CGFloat width = UIViewNoIntrinsicMetric;
	
	CGFloat height = self.bunBottomImage.size.height + self.bunTopImage.size.height;
	height += self.hasLettuce ? self.lettuceImage.size.height : 0;
	height += self.hasCheese ? self.cheeseImage.size.height : 0;
	height += self.numberOfPatties * self.pattyImage.size.height;
	height = ceilf(height);
	
	return CGSizeMake(width, height);
}

@end
