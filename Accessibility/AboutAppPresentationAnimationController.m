//
//  AboutAppPresentationAnimationController.m
//  Accessibility
//
//  Created by Izzy Fraimow on 10/9/14.
//  Copyright (c) 2014 anathemacalculus. All rights reserved.
//

#import "AboutAppPresentationAnimationController.h"
#import "AppTheme.h"

@implementation AboutAppPresentationAnimationController

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext {
	return 0.5;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext {
	if(UIAccessibilityIsReduceMotionEnabled()) {
		if(self.presenting) {
			[self fadeInWithContext:transitionContext];
		} else {
			[self fadeOutWithContext:transitionContext];
		}
		
		return;
	}
	
	if(self.presenting) {
		[self presentWithContext:transitionContext];
	} else {
		[self dismissWithContext:transitionContext];
	}
}

- (void)fadeInWithContext:(id <UIViewControllerContextTransitioning>)transitionContext {
	UIView *toView = [transitionContext viewForKey:UITransitionContextToViewKey];
	UIView *containerView = [transitionContext containerView];
	
	toView.center = containerView.center;
	toView.alpha = 0;
	
	[containerView addSubview:toView];
	
	[AppTheme animateStandard:^{
		toView.alpha = 1;
	} completion:^(BOOL finished) {
		[transitionContext completeTransition:YES];
	}];
}

- (void)fadeOutWithContext:(id <UIViewControllerContextTransitioning>)transitionContext {
	UIView *presentedView = [transitionContext viewForKey:UITransitionContextFromViewKey];
	UIView *containerView = [transitionContext containerView];
	
	[containerView addSubview:presentedView];
	
	[AppTheme animateStandard:^{
		presentedView.alpha = 0;
	} completion:^(BOOL finished) {
		[transitionContext completeTransition:YES];
	}];
}

- (void)presentWithContext:(id <UIViewControllerContextTransitioning>)transitionContext {
	UIView *toView = [transitionContext viewForKey:UITransitionContextToViewKey];
	UIView *containerView = [transitionContext containerView];
	
	toView.center = containerView.center;
	toView.transform = CGAffineTransformScale(CGAffineTransformIdentity, .1, .1);
	
	[containerView addSubview:toView];
	
	[AppTheme animatePresentation:^{
		toView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
	} completion:^(BOOL finished) {
		[transitionContext completeTransition:YES];
	}];
}

- (void)dismissWithContext:(id <UIViewControllerContextTransitioning>)transitionContext {
	UIView *presentedView = [transitionContext viewForKey:UITransitionContextFromViewKey];
	UIView *containerView = [transitionContext containerView];
	
	[containerView addSubview:presentedView];
	
	[AppTheme animatePresentation:^{
		presentedView.transform = CGAffineTransformScale(CGAffineTransformIdentity, .01, .01);
	} completion:^(BOOL finished) {
		[transitionContext completeTransition:YES];
	}];
}

@end
