//
//  main.m
//  Accessibility
//
//  Created by Izzy Fraimow on 9/23/14.
//  Copyright (c) 2014 anathemacalculus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
