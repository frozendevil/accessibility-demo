//
//  AboutViewController.m
//  Accessibility
//
//  Created by Izzy Fraimow on 10/9/14.
//  Copyright (c) 2014 anathemacalculus. All rights reserved.
//

#import "AboutViewController.h"

#import "AboutAppPresentationController.h"
#import "AboutAppPresentationAnimationController.h"

@interface AboutViewController ()

@property (nonatomic, strong) UITapGestureRecognizer *tapRecognizer;

@end

@implementation AboutViewController

- (id)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	if(!self) { return nil; }
	
	self.modalPresentationStyle = UIModalPresentationCustom;
	self.transitioningDelegate = self;
	
	return self;
}

- (void)viewDidLoad {
	self.tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundTapped)];
	self.tapRecognizer.numberOfTapsRequired = 1;
	
	[self.view addGestureRecognizer:self.tapRecognizer];
}

- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source {
	if(presented == self) {
		AboutAppPresentationController *presentationController = [[AboutAppPresentationController alloc] initWithPresentedViewController:presented presentingViewController:presenting];
		return presentationController;
	} else {
		return nil;
	}
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
	if(presented == self) {
		AboutAppPresentationAnimationController *animationController = [[AboutAppPresentationAnimationController alloc] init];
		animationController.presenting = YES;
		return animationController;
	} else {
		return nil;
	}
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
	if(dismissed == self) {
		AboutAppPresentationAnimationController *animationController = [[AboutAppPresentationAnimationController alloc] init];
		animationController.presenting = NO;
		return animationController;
	} else {
		return nil;
	}
}

- (void)backgroundTapped {
	[self dismissViewControllerAnimated:YES completion:nil];
}

@end
