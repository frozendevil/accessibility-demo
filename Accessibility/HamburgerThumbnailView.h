//
//  HamburgerThumbnailView.h
//  Accessibility
//
//  Created by Izzy Fraimow on 10/7/14.
//  Copyright (c) 2014 anathemacalculus. All rights reserved.
//

/// This class is mostly sloppy drawing code, nothing important here

@import UIKit;

IB_DESIGNABLE
@interface HamburgerThumbnailView : UIView

@property (nonatomic, assign) IBInspectable BOOL hasCheese;
@property (nonatomic, assign) IBInspectable BOOL hasLettuce;
@property (nonatomic, assign) IBInspectable NSUInteger numberOfPatties;

//these should be private, ignore this
@property (nonatomic, strong) UIImage *bunTopImage;
@property (nonatomic, strong) UIImage *bunBottomImage;
@property (nonatomic, strong) UIImage *pattyImage;
@property (nonatomic, strong) UIImage *cheeseImage;
@property (nonatomic, strong) UIImage *lettuceImage;

@end
