//
//  HamburgerCollectionViewCell.m
//  Accessibility
//
//  Created by Izzy Fraimow on 10/7/14.
//  Copyright (c) 2014 anathemacalculus. All rights reserved.
//

#import "HamburgerCollectionViewCell.h"

@implementation HamburgerCollectionViewCell

- (id)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	if(!self) { return nil; }
	
	self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
	self.titleLabel.preferredMaxLayoutWidth = 300;
	
	return self;
}

- (void)prepareForReuse {
	[self.thumbnailView setNeedsDisplay];
}

//- (UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes {
//    UICollectionViewLayoutAttributes *attributes = [layoutAttributes copy];
//    CGSize size = CGSizeMake(150, 150);//[self.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
//    CGRect newFrame = attributes.frame;
//    newFrame.size.height = size.height;
//    attributes.frame = newFrame;
//    return attributes;
//}

@end
