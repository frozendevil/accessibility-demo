//
//  AppTheme.m
//  Accessibility
//
//  Created by Izzy Fraimow on 10/8/14.
//  Copyright (c) 2014 anathemacalculus. All rights reserved.
//

#import "AppTheme.h"

@implementation AppTheme

+ (instancetype)sharedTheme {
	static AppTheme *theme;
	
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		theme = [[AppTheme alloc] init];
	});
	
	return theme;
}

+ (void)animateStandard:(dispatch_block_t)animation completion:(void (^)(BOOL finished))completion {
	[UIView animateWithDuration:1.0 animations:animation completion:completion];
}

+ (void)animatePresentation:(dispatch_block_t)animation completion:(void (^)(BOOL finished))completion {
	if(UIAccessibilityIsReduceMotionEnabled()) {
		NSLog(@"###########################################################################################");
		NSLog(@"# this is a spring based animation and may be inappropriate when reduce motion is enabled #");
		NSLog(@"###########################################################################################");
	}
	
	[UIView animateWithDuration:1.0
						  delay:0
		 usingSpringWithDamping:0.5
		  initialSpringVelocity:0.1
						options:0
					 animations:animation
					 completion:completion];
}

@end
