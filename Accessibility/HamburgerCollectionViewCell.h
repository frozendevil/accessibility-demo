//
//  HamburgerCollectionViewCell.h
//  Accessibility
//
//  Created by Izzy Fraimow on 10/7/14.
//  Copyright (c) 2014 anathemacalculus. All rights reserved.
//

@import UIKit;

#import "HamburgerThumbnailView.h"

@interface HamburgerCollectionViewCell : UICollectionViewCell

@property (nonatomic, assign) IBOutlet HamburgerThumbnailView *thumbnailView;
@property (nonatomic, assign) IBOutlet UILabel *titleLabel;

@end
